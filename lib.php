<?php
    session_start();

    /* pendefinisian konstanta untuk delimiter bagi config.txt dan data.csv (karakter pemisah field) */
    define("DL",";");

    /* mengecek apakah user sudah login atau belum saat mau mengakses halaman. 
       jika belum, akan "ditendang" ke halaman login sambil diberi pesan.
    */
    function cek_login() {
        if (!isset($_SESSION['nik'])) {
            $pesan = "Anda tidak memiliki akses ke halaman tersebut!<br>";
            $pesan .= "Silakan login terlebih dahulu.";
            set_msg($pesan,'danger');
            header("Location: login.php"); exit;
        }
    }

    /* membuat session untuk menampilkan pesan, baik di halaman yang sama maupun lintas halaman.
       pesan akan ditampilkan oleh function show_msg(). Parameter $type bisa menggunakan styling 
       standar bootstrap: warning, danger, primary, dsb.
    */
    function set_msg($text, $type='success') {
        $_SESSION['msg-text'] = $text;
        $_SESSION['msg-type'] = $type;
    }   

    /* menampilkan pesan berdasar ada dan tidaknya session msg-text, yang diset melalui set_msg(), 
       lalu meng-unset session setelah menampilkannya. Semacam flash message gitu.
    */
    function show_msg() {
        if(isset($_SESSION['msg-text'])) {
            echo '<div class="alert alert-'.$_SESSION['msg-type'].'">';
            echo $_SESSION['msg-text'].'</div>';
            unset($_SESSION['msg-text']);
            unset($_SESSION['msg-type']);
        }    
    }

    /* mengubah tanggal dalam format ISODate (yyyy-mm-dd) ke dalam format Indonesia (dd-mm-yyyy) */    
    function tgl_indo($tgl) {
        $t = explode('-',$tgl);
        return $t[2].'-'.$t[1].'-'.$t[0];
    }

    /* mengecek apakah nik sudah terdapat di file config ataukah belum. Jika sudah, return true. */
    function nik_exists($nik) {
        $ketemu = false;
        error_reporting(E_ALL^E_WARNING);
        $f = fopen('config.txt','r');
        if ($f) {
            while(!feof($f)) {
                $r = explode(DL, fgets($f)); 
                if ($r[0] == $nik)
                    $ketemu = true;                    
            }
            fclose($f);
        }    
        return $ketemu;
    }

    /* membersihkan string dari whitespace di kanan dan kiri (trim()), serta menghilangkan 
       karakter yang digunakan sebagai delimiter, jika diketikkan pengguna 
    */
    function clean($str) {
        return trim(str_replace(DL,'',$str));
    }