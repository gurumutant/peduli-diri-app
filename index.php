<?php
    include("lib.php");
    cek_login();
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Peduli Diri App</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" 
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  </head>
  <body>
    <h1>Peduli Diri</h1>  
    <nav class="nav justify-content-center|justify-content-end">
      <a class="nav-link active disabled" href="#">Home</a>
      <a class="nav-link" href="catatan.list.php">Catatan Perjalanan</a>
      <a class="nav-link" href="catatan.form.php">Isi Data</a>
      <a class="nav-link" href="logout.php">Logout</a>
    </nav>
    <?php show_msg(); ?>
    <div class="p-5 bg-light">
        <div class="container">
            <p class="lead">
              Selamat datang <?= $_SESSION['nama'] ?> di Aplikasi Peduli Diri
            </p>
            <p class="lead">
                <a class="btn btn-primary btn-lg" href="catatan.form.php"
                 role="button">Isi Catatan Perjalanan</a>
            </p>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
