<?php
// Hanya contoh bahwa pengacakan menggunakan function password_hash() 
// menghasilkan  hash yang berbeda-beda setiap kali run, namun akan
// selalu bisa diverifikasi menggunakan password_verify() 
$acak = password_hash('rahasia', PASSWORD_BCRYPT);
echo $acak;
echo "<br>";
echo (password_verify('rahasia',$acak) == true) ? 
    "Password sesuai" : "password tidak sesuai";