<?php
  include("lib.php");
  if (isset($_POST['nik'])) { // apakah ada pengiriman form
    extract($_POST); // memecah variabel $_POST
    if ((trim($nik) == '') || (trim($password) == '')) {
      set_msg("NIK dan Password tidak boleh kosong!","danger");
    } else {
      error_reporting(E_ALL^E_WARNING);
      $f = fopen('config.txt','r');
      if ($f) {
        while(!feof($f)) {
          $r = explode(DL, fgets($f)); 
          if (($r[0] == $nik) && (password_verify($password, $r[1]))) {
            $_SESSION['nik'] = $nik; 
            $_SESSION['nama'] = $r[2]; 
            set_msg('Login Berhasil!');
            header("Location: index.php"); exit;
          }     
        }
        set_msg('NIK atau password salah!','warning');
      } else {
        set_msg('File config.txt belum ada. Silakan registrasi dulu.','warning');
      }
    }
  }
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Login Page</title>
  </head>
  <body>
    <h1 class="text-center">Silakan Login</h1>
    <?php show_msg(); ?>
    <div class="row justify-content-center">
      <div class="col-lg-4 col-sm-6">
          <form action="" method="post">
              <input type="text" name="nik" 
                id="nik" class="form-control mb-2" placeholder="NIK">
              <input type="password" name="password" 
                id="password" class="form-control mb-2" placeholder="Password">
              <a href="daftar.php" class="btn btn-secondary">Saya Pengguna Baru</a>   
              <input type="submit" value="Masuk" class="btn btn-primary float-end">    
          </form>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
