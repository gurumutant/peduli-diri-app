<?php
    include("lib.php");
    cek_login();
    // mendeteksi pengiriman form
    if (isset($_POST['tanggal'])) { 
        extract($_POST); 
        $f = fopen('data.csv', 'a+');
        $tanggal = clean($tanggal);
        $jam = clean($jam);
        $lokasi = clean($lokasi);            
        $suhu = clean($suhu);  
        $data = $tanggal.DL.$jam.DL.$lokasi.DL.$suhu.PHP_EOL;
        // validasi form, server-side  
        if ($tanggal=='' || $jam=='' || $lokasi=='' || $suhu=='') {
            set_msg('ERROR: Ada isian form yang masih kosong!','danger');
        } else {
          if(fwrite($f, $data) === FALSE) {
              die("File gagal ditulis!");
          } else {
              set_msg('Catatan perjalanan berhasil disimpan');
              header("Location: catatan.list.php"); 
              fclose($f);
              exit;
          }
        }  
        fclose($f);
    }
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Peduli Diri App</title>
    <meta charset="utf-8">
    <meta name="viewport" 
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- bootstrap.min.css CDN -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  </head>
  <body>
    <h1>Peduli Diri</h1>  
    <nav class="nav justify-content-center|justify-content-end">
      <a class="nav-link" href="index.php">Home</a>
      <a class="nav-link" href="catatan.list.php">Catatan Perjalanan</a>
      <a class="nav-link active disabled" href="catatan.form.php">Isi Data</a>
      <a class="nav-link" href="logout.php">Logout</a>
    </nav>
    <h4>Entry Catatan Perjalanan</h4>
    <?php show_msg(); ?>
    <div class="row justify-content-center">
      <div class="col-lg-4 col-sm-6">
          <form action="" method="post">
              <label class="form-label" for="tanggal">Tanggal</label>
              <input type="date" name="tanggal" 
                id="tanggal" class="form-control mb-2 w-75"
                value="<?= (isset($_POST['tanggal'])) ? $_POST['tanggal'] : ''; ?>">
              <label class="form-label" for="jam">Jam</label>  
              <input type="time" name="jam" 
                id="jam" class="form-control mb-2 w-25"
                value="<?= (isset($_POST['jam'])) ? $_POST['jam'] : ''; ?>">
              <input type="text" name="lokasi" 
                id="lokasi" class="form-control mb-2" 
                placeholder="Tempat yang dikunjungi"
                value="<?= (isset($_POST['lokasi'])) ? $_POST['lokasi'] : ''; ?>">
              <div class="input-group w-50">
                <input type="text" name="suhu" 
                    id="suhu" class="form-control mb-2" 
                    placeholder="Suhu tubuh"
                    value="<?= (isset($_POST['suhu'])) ? $_POST['suhu'] : ''; ?>">
                <div class="input-group-text mb-2">&deg; C</div>
              </div>
              <input type="submit" value="Simpan" 
                class="btn btn-primary float-end">    
          </form>
      </div>
    </div>
    <!-- bootstrap.bundle.min.js CDN -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
