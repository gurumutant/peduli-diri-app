<?php
    include("lib.php");
    cek_login();   
    error_reporting(E_ALL^E_WARNING);
    $f = fopen('data.csv', 'r');
    if ($f) {
      while(!feof($f)) {
          $r = explode(DL, fgets($f)); // memecah string menjadi array
          if (count($r) == 4) // memastikan array-nya ada 3 elemen
              $rows[] = $r; // menambahkan ke array $rows
      }            
      fclose($f);  
    } else {
      set_msg('Belum ada data catatan perjalanan. Silakan isi catatan perjalanan lebih dahulu!','warning');
      $rows = [];
    }  
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Peduli Diri App</title>
    <meta charset="utf-8">
    <meta name="viewport" 
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- bootstrap.min.css CDN -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- Include JQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <!-- include Datatables CDN -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.4/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.js"></script>
  </head>
  <body>
    <h1>Peduli Diri</h1>  
    <nav class="nav justify-content-center|justify-content-end">
      <a class="nav-link" href="index.php">Home</a>
      <a class="nav-link active disabled" href="catatan.list.php">
        Catatan Perjalanan</a>
      <a class="nav-link" href="catatan.form.php">Isi Data</a>
      <a class="nav-link" href="logout.php">Logout</a>
    </nav>
    <h4>Daftar Catatan Perjalanan</h4>
    <?php show_msg(); ?>
    <br>
    <div class="row justify-content-center">
      <div class="col-lg-10 col-sm-12">
          <table id="catatan">
              <thead>
                  <tr>
                      <th>Tanggal</th>
                      <th>Waktu</th>
                      <th>Lokasi</th>
                      <th>Suhu Tubuh</th>
                  </tr>
              </thead>
              <tbody>
                <?php 
                    $i = 1;
                    foreach($rows as $row) { 
                ?>
                  <tr>
                      <td><?= tgl_indo($row[0]) ?></td>
                      <td><?= $row[1] ?></td>
                      <td><?= $row[2] ?></td>
                      <td><?= $row[3] ?></td>
                  </tr>
                <?php } ?>  
              </tbody>
          </table>
          <a class="btn btn-primary btn-lg float-end" href="catatan.form.php"
            role="button">Isi Catatan Perjalanan</a>       
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script>
        $(document).ready( function () {
            $('#catatan').DataTable();
        });
    </script>
  </body>
</html>
