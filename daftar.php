<?php
  include("lib.php");
  if (isset($_POST['nik'])) {
    extract($_POST);
    if ((trim($nik) == '') || (trim($password) == '') || (trim($nama) == '')) {
      set_msg("NIK, Password, dan Nama tidak boleh kosong!","danger");
    } else {
      if (!nik_exists($nik)) {
        $f = fopen('config.txt', 'a+');
        $data = clean($nik).DL;
        $data .= password_hash($password, PASSWORD_BCRYPT).DL;
        $data .= clean($nama).PHP_EOL;
        if(fwrite($f, $data) === FALSE) {
          set_msg('Gagal menulis ke data akun!','danger');
          fclose($f);
        } else {
          set_msg('Registrasi berhasil. Silakan gunakan untuk login.');
          header("Location: login.php"); fclose($f); exit;
        }
      } else {
          set_msg('NIK sudah ada di sistem. Silakan login, atau gunakan NIK yang lain.','warning');  
      }
    }
  }
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Registration Page</title>
  </head>
  <body>
    <h1 class="text-center">Pendaftaran Akun</h1>
    <?php show_msg(); ?>
    <div class="row justify-content-center">
      <div class="col-lg-4 col-sm-6">
          <form action="" method="post">
              <input type="text" name="nik" 
                id="nik" class="form-control mb-2" placeholder="NIK">
              <input type="password" name="password" 
                id="password" class="form-control mb-2" placeholder="Password">
              <input type="text" name="nama" 
                id="nama" class="form-control mb-2" placeholder="Nama Lengkap">
              <input type="submit" value="Daftar" class="btn btn-primary float-end">    
          </form>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>
