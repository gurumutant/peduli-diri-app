# Peduli-diri-app

Contoh paling sederhana pengerjaan Soal UKK RPL 2022 Paket 1, menggunakan operasi file teks untuk menyimpan catatan perjalanan. Mekanisme login dan penyimpanan data hanya menggunakan file teks. 

Library pihak ke tiga yang dimanfaatkan dalam project ini (via CDN):
- Bootstrap 5.1.3
- jQuery 3.6.0
- Datatables 1.11.4

File data.csv yang dihasilkan akan bisa dibuka dan diedit menggunakan Microsoft Excel dengan baik, jika diasumsikan Anda menggunakan regional setting Indonesia. Namun jika menggunakan regional setting English (US) atau semacamnya, cukup ganti delimiter menjadi tanda koma (,). Anda akan diperlukan untuk menghapus config.txt dan data.csv jika mengubah delimiter di lib.php

# Instalasi

Cukup _extract_ di folder yang berada di bawah DocumentRoot (xampp htdocs, atau laragon www), lalu akses menggunakan http://localhost/nama_folder . File teks config.txt akan dibuat secara otomatis saat ada registrasi pertama, dan data.csv akan dibuat saat ada isian catatan perjalanan yang disimpan pertama kalinya.

# Konfigurasi

Pastikan delimiter pada konstanta "DL" sesuai dengan yang Anda inginkan. Sesuaikan jika perlu saja. 

# TODO
- penambahan jQuery input mask untuk memaksa input suhu selalu dalam format desimal
